// @ts-ignore
import { getBikeRoutesList, getBikeRoutesByDifficult } from "lib_bike_routes/BikeRoutesLib";

export const bikeRoutesList = getBikeRoutesList();
export const bikeRoutesItems = bikeRoutesList.list;
export const totalRoutes = bikeRoutesList.total;
export const routesById = bikeRoutesItems.reduce((current, item) => {
  current[item.id] = item;
  return current;
}, {});
export const routesByDifficult = getBikeRoutesByDifficult();

// TODO: remover dos lugares que é usado e usar o "routesDifficult"
export const routesDificult = {
  1: 'Passeio',
  2: 'Fácil',
  3: 'Médio',
  4: 'Difícil',
  5: 'Muito difícil',
}

export const routesDifficult = {
  1: {
    title: 'Passeio',
    description: 'Para quem está começando a andar de bike e mesmo que não tenha uma, as bikes compartilhadas suportam o passeio.',
  },
  2: {
    title: 'Fácil',
    description: 'Se já anda de bike e quer explorar São Paulo, sem maiores dificuldades. As rotas também podem ser feitas com bikes compartilhadas.',
  },
  3: {
    title: 'Médio',
    description: 'Para quem anda casualmente e tem bike própria, as rotas mais longas ou com mais subidas que as de nível fácil.',
  },
  4: {
    title: 'Difícil',
    description: 'Pedalar já está no seu dia-à-dia! Trajetos que apresentam trechos com subidas, exigindo um bom preparo físico.',
  },
  5: {
    title: 'Muito difícil',
    description: 'Indicado para quem já treina a bastante tempo, trajetos longos e com boas subidas durante o percurso, esteja bem preparado e com o condicionamente físico em dia!',
  },
}