import React from "react"
import { useParams } from "react-router-dom"
import './styles/index';
import { routesById } from "./helpers";
import { ModalBikeRouteDetails } from "./ModalBikeRouteDetail";
import { CardListByDifficult } from "./CardListByDifficult";

export default () => {
  const params = useParams();

  return (
    <div className='mf-bike-route--bike-routes--page'>
      <div className="cards-container">
        <CardListByDifficult />
        {params.id ? <ModalBikeRouteDetails bikeRoute={routesById[params.id]} /> : null}
      </div>
    </div >
  )
}