import React, { useEffect, useRef, useState } from "react";
import { isInViewport } from "../../../utils";
import { CardRoute } from "../CardRoute";
import { routesByDifficult, routesDifficult } from "../helpers";
import "./styles.css";

const CardListWrapper = ({ byDifficult = [] }) => {
  const [useScroll, setUseScroll] = useState(false);
  const wrapperRef = useRef();
  const allCardItems = useRef([]);
  const routesQtt = byDifficult.length;

  const setUseScrollOnResizeScreen = () => {
    const wrapperEl = wrapperRef.current as HTMLElement;
    setUseScroll(wrapperEl.offsetWidth !== wrapperEl.scrollWidth);
  }

  useEffect(() => {
    const wrapperEl = wrapperRef.current as HTMLElement;
    allCardItems.current = [...wrapperEl.querySelectorAll('.card-item')];
    setUseScroll(wrapperEl.offsetWidth !== wrapperEl.scrollWidth);

    window.addEventListener('resize', setUseScrollOnResizeScreen);

    return () => window.removeEventListener('resize', setUseScrollOnResizeScreen);
  }, [])

  const handleChangeIndex = (
    type: 'back' | 'next',
    e?: React.MouseEvent<HTMLSpanElement, MouseEvent>
  ) => {
    e?.preventDefault();
    e?.stopPropagation();

    const cards = allCardItems.current as HTMLElement[];
    const inlineType = type === 'back' ? "end" : "start";
    let nextIndex = null
    if (type === 'back') {
      cards.some((card, index) => {
        const currentIsInViewPort = isInViewport(card);
        if (!currentIsInViewPort) {
          nextIndex = index;
          return false;
        }
        if (index === 0) nextIndex = routesQtt - 1;
        return true;
      })
    }
    else {
      cards.some((_, index) => {
        const newIndex = routesQtt - 1 - index;
        const card = cards[newIndex];
        const currentIsInViewPort = isInViewport(card);
        if (!currentIsInViewPort) return false;
        nextIndex = newIndex + 1 > routesQtt - 1 ? 0 : newIndex + 1;
        return true;
      })
    }
    typeof nextIndex === 'number' && cards[nextIndex].scrollIntoView({ inline: inlineType, block: "nearest" });
  }

  return (
    <div className="card-horizontal-list-wrapper">
      <hr className="card-horizontal-list-line" />
      <div>
        <div
          ref={wrapperRef}
          className="cards-horizontal-list-container custom-scroll custom-scroll-light mf-container"
        >

          {byDifficult.map(route => (
            <CardRoute key={route.id} route={route} showDifficult={false} />
          ))}
        </div>
      </div>
      {
        useScroll ?
          <>
            <span
              onClick={(e) => { handleChangeIndex('back', e); }}
              className="action-btn back-btn">&#60;
            </span>
            <span
              onClick={(e) => { handleChangeIndex('next', e); }}
              className="action-btn next-btn">&#62;
            </span>
          </> : null
      }
    </div>
  )
}

const CardListByDifficult = () => {
  return routesByDifficult.list.map((byDifficult, index) => (
    <div className="cards-horizontal-section" key={index}>
      <div className="card-horizontal-section-title-container mf-container">
        <h3>{routesDifficult[index + 1].title}</h3>
        <p>{routesDifficult[index + 1].description}</p>
      </div>
      <CardListWrapper byDifficult={byDifficult} />
    </div>
  ))
}

export { CardListByDifficult }