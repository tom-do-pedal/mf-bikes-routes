import React from 'react';
import { useNavigate } from "react-router-dom";
import { Carousel } from "../../../components/Carousel"
import { ModalWrapper } from "../../../components/ModalWrapper"
import { routesDificult } from "../helpers"
import { translateDays } from './helpers';
import "./styles.modal.css"

const ModalBikeRouteDetails = ({ bikeRoute }) => {
  const navigate = useNavigate();

  return <ModalWrapper onClose={() => navigate(`/bike-routes`)}>
    <>
      <h4>{bikeRoute.routeName}</h4>
      <div className="modal-container-header-infos">
        <span>Dificuldade: {routesDificult[bikeRoute.difficulty]} ({bikeRoute.distance})</span>
        <a href={bikeRoute.routeUrl} target="_blank" onClick={(e) => e.stopPropagation()} >Veja o mapa da rota &#62;</a>
      </div>
      <div className="bike-routes-detail-content custom-scroll">
        <p>{bikeRoute.description}</p>
        <Carousel
          classNameContainer="bike-routes-detail-gallery"
          items={bikeRoute.galleryImgs.map(img => ({
            imgUrl: img.url,
            title: img.title,
            // description: img.description
          }))}
        />

        <div className="bike-routes-detail-highlight-places">
          <h5>Destaques da rota</h5>
          <ul>
            {
              bikeRoute.routeHighlights.map((highlight, index) => <li key={index}>{highlight}</li>)
            }
          </ul>
        </div>

        {
          bikeRoute.recommendedDays ?
            <div className="bike-routes-detail-recomendaded-days">
              <h5>Dias recomendados</h5>
              <ul>
                {
                  bikeRoute.recommendedDays.map((dayKey) => <li key={dayKey}>{translateDays[dayKey]}</li>)
                }
              </ul>
            </div> : null
        }

        <div className="bike-routes-detail-route-point">
          <h5>Ponto de encontro</h5>
          <ul >
            {
              bikeRoute.routePoints.start === bikeRoute.routePoints.end ?
                <li><b>Início e fim</b>: {bikeRoute.routePoints.start}</li> :
                <>
                  <li><b>Início</b>: {bikeRoute.routePoints.start}</li>
                  <li><b>Fim</b>: {bikeRoute.routePoints.end}</li>
                </>
            }
          </ul>
        </div>
      </div>

    </>
  </ModalWrapper >
}

export { ModalBikeRouteDetails }