import React from "react"
import { useNavigate } from "react-router-dom";
import { routesDificult } from "../helpers"
import "./styles.card-item.css"

const CardRoute = ({ route, showDifficult = true, showDistance = true }) => {
  const navigate = useNavigate();
  return (
    <div
      key={route.id}
      className={`card-item card-item-${route.id}`}
      onClick={() => navigate(`/bike-routes/${route.id}`)}
    >
      <h4 className="card-title">{route.routeName}</h4>
      {showDifficult ?
        <span className="card-tag card-tag-difficulty">{routesDificult[route.difficulty]}</span>
        : null
      }
      {showDistance ?
        <span className="card-tag card-tag-distance">{route.distance}</span>
        : null
      }
      <div className="card-image-container" style={{
        backgroundImage: `url(${route.thumbnaillUrl})`
      }}>
        <a href={route.routeUrl} target="_blank" onClick={(e) => e.stopPropagation()}>Mapa da rota</a>
      </div>
    </div>
  )
}

export { CardRoute }