import React from "react";
import ReactDOM from "react-dom";
import { AppRoutes } from "./routes";
import "./styles.css";
import "tom_do_pedal_ui/CommomStyles";

const App = () => <AppRoutes />

export const mount = (selector: string | HTMLElement) => {
  const rootElement = typeof selector === 'string' ? document.querySelector(selector) : selector
  ReactDOM.render(<App />, rootElement);
}

const DEV_ROOT_SELECTOR = '#app-bike-routes'
if (process.env.NODE_ENV === 'development') {
  const devRoot = document.querySelector(DEV_ROOT_SELECTOR);
  if (devRoot) {
    mount(DEV_ROOT_SELECTOR);
  }
}

