import { ReactNode } from "react";

export interface ModalWrapperProps {
  onClose: () => void;
  children: ReactNode;
  classNameWrapper?: string;
  classNameModal?: string;
}