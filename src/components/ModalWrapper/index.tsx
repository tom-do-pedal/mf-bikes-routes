import React, { useEffect } from "react"
import { ModalWrapperProps } from "./interfaces"
import './styles.css'

const ModalWrapper = ({ onClose, classNameWrapper = '', classNameModal = '', children }: ModalWrapperProps) => {
  useEffect(() => {
    document.querySelector('body').classList.add('open-modal')
  }, [])

  const handleClose = () => {
    document.querySelector('body').classList.remove('open-modal')
    onClose()
  }

  return <div className={`modal-wrapper ${classNameWrapper}`} onClick={handleClose}>
    <div className={`modal-container ${classNameModal}`} onClick={(e) => e.stopPropagation()}>
      <button className="modal-close-btn" onClick={handleClose}>x</button>
      {children}
    </div>
  </div>
}

export { ModalWrapper }