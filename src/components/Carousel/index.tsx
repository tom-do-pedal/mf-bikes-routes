import React, { useRef, useState } from "react";
import { CarouselProps } from "./interfaces";
import './styles.css'

const Carousel = ({ items, classNameContainer = '', classNameWrapper = '' }: CarouselProps) => {
  const loadedImages = useRef(0);
  const currentItemIndex = useRef(0);
  const maxItems = items.length;
  const [isLoading, setIsLoading] = useState(true)

  const scrollNextImageIntoView = (index: number) =>
    document
      .querySelectorAll('.carousel-container .picture-wrapper')[index]
      .scrollIntoView({ block: "nearest" });


  const handleLoading = () => {
    const nextLoadedImages = loadedImages.current + 1
    loadedImages.current = nextLoadedImages;

    if (nextLoadedImages >= maxItems) {
      setIsLoading(false);
      currentItemIndex.current = 0;
      scrollNextImageIntoView(0);
    }
  }

  const handleChangeImage = (type: 'next' | 'back') => {
    const currentIndex = currentItemIndex.current;
    const index = type === 'next' ?
      currentIndex + 1 >= maxItems ? 0 : currentIndex + 1 :
      currentIndex - 1 < 0 ? maxItems - 1 : currentIndex - 1;
    scrollNextImageIntoView(index);
    currentItemIndex.current = index;
  }

  return <div className={`carousel-wrapper ${classNameWrapper}`}>
    <span className="action-btn back-action-btn" onClick={() => handleChangeImage('back')}>&#60;</span>
    <div className={`carousel-container custom-scroll custom-scroll-light ${classNameContainer}`}>
      {
        items.map(({ imgUrl, title, description }) => (
          <picture key={imgUrl} title={title} className="picture-wrapper">
            <img src={imgUrl} alt={title} onLoad={handleLoading} />
            <p className="picture-title">{title}</p>
            {description ? <p>{description}</p> : null}
          </picture>
        ))
      }
    </div>
    <span className="action-btn next-action-btn" onClick={() => handleChangeImage('next')}>&#62;</span>
  </div >
}

export { Carousel }