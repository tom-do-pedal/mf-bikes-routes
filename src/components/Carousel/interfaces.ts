export interface CarouselProps {
  items: {
    imgUrl: string;
    title?: string;
    description?: string;
  }[],
  classNameContainer?: string;
  classNameWrapper?: string;
}