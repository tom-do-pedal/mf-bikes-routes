import React, { lazy, Suspense } from "react";
import {
  BrowserRouter as Router,
  Route,
  Routes,
} from "react-router-dom";

const BikeRoutes = lazy(() => import('../pages/BikeRoutes'));

export const AppRoutes = () => {
  return (
    <Suspense fallback={<div>Carregando...</div>}>
      <Router>
        <Routes>
          <Route path="/bike-routes/:id" element={<BikeRoutes />} />
          <Route path="/bike-routes/" element={<BikeRoutes />} />
        </Routes>
      </Router>
    </Suspense>
  )
}