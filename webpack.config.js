const HtmlWebPackPlugin = require("html-webpack-plugin");
const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");

const deps = require("./package.json").dependencies;
module.exports = (env) => ({
  output: {
    publicPath: env.production ?
      "https://tom-do-pedal-mf-bike-routes.netlify.app/" :
      "http://localhost:3001/",
  },

  resolve: {
    extensions: [".tsx", ".ts", ".jsx", ".js", ".json"],
  },

  devServer: {
    port: 3001,
    historyApiFallback: true,
  },

  module: {
    rules: [
      {
        test: /\.m?js/,
        type: "javascript/auto",
        resolve: {
          fullySpecified: false,
        },
      },
      {
        test: /\.(css|s[ac]ss)$/i,
        use: ["style-loader", "css-loader", "postcss-loader"],
      },
      {
        test: /\.(ts|tsx|js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
    ],
  },

  plugins: [
    new ModuleFederationPlugin({
      name: "bike_routes",
      filename: "remoteEntry.js",
      remotes: {
        "lib_bike_routes": env.production || true ?
          "lib_bike_routes@https://tom-do-pedal-bike-routes-lib.netlify.app/remoteEntry.js" :
          "lib_bike_routes@http://localhost:8888/remoteEntry.js",
        "tom_do_pedal_ui": env.production || true ?
          "tom_do_pedal_ui@https://tom-do-pedal-ui.netlify.app/remoteEntry.js" :
          "tom_do_pedal_ui@http://localhost:8181/remoteEntry.js"
      },
      exposes: {
        "./BikeRoutesApp": "./src/App"
      },
      shared: {
        ...deps,
        react: {
          singleton: true,
          requiredVersion: deps.react,
        },
        "react-dom": {
          singleton: true,
          requiredVersion: deps["react-dom"],
        },
      },
    }),
    new HtmlWebPackPlugin({
      template: "./src/index.html",
    }),
  ],
});
